import subprocess

data = subprocess.check_output(['netsh', 'wlan', 'show', 'profiles']).decode('utf-8', errors="backslashreplace").split('\n')
profiles = [i.split(":")[1][1:-1] for i in data if "All User Profile" in i]
for i in profiles:
    try:
        results = subprocess.check_output(['netsh', 'wlan', 'show', 'profile', i, 'key=clear']).decode('utf-8', errors="backslashreplace").split('\n')
        results = [b.split(":")[1][1:-1] for b in results if "Key Content" in b]
        try:
            print ("{:<30}|  {:<}".format(i, results[0]))
        except IndexError:
            print ("{:<30}|  {:<}".format(i, ""))
    except subprocess.CalledProcessError:
        print ("{:<30}|  {:<}".format(i, "ENCODING ERROR"))
input("")

# import subprocess

# data = subprocess.check_output(['netsh','wlan','show','profile']).decode('utf-8').split('\n')
# wifis = [line.split(':')[1][1:-1]  for line in data if ('All User Profile') in line]

# for wifi in wifis:
#     results = subprocess.check_output(['netsh','wlan','show','profile',wifi,'key=clear']).decode('utf-8').split('\n')
#     results = [line.split(':')[1][1:-1]  for line in results if ('Key Content') in line]
#     try:
#         print(f'Name: {wifi},\t\t\tPassword: {results[0]}\n')
#     except IndexError:
#         print(f'Name: {wifi},\t\t\tPassword: Cannot be read\n')

        

# importing random
# from random import *

# # taking input from user
# user_pass = input("Enter your password")

# # storing alphabet letter to use thm to crack password
# password = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j','k', 
#             'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't','u','v', 
#             'w', 'x', 'y', 'z','']

# # initializing an empty string
# guess = ""


# # using while loop to generate many passwords untill one of
# # them does not matches user_pass
# while (guess != user_pass):
#     guess = ""
#     # generating random passwords using for loop
#     for letter in range(len(user_pass)):
#         guess_letter = password[randint(0, 25)]
#         guess = str(guess_letter) + str(guess)
#     # printing guessed passwords
#     print(guess)
    
# # printing the matched password
# print("Your password is",guess)