import pytube
import youtube_dl
from youtube_dl import options

def MP3downloads():
    #input url 
    video_url = input("Enter the song's URL: ")
    
    video_info = youtube_dl.YoutubeDL().extract_info(url = video_url,download=False)
    # characteristic
    options={
        'format': 'bestaudio/best',
        'keepvideo': False,
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '320',
        }]
    }
    with youtube_dl.YoutubeDL(options) as ytdl:
        ytdl.download([video_info['webpage_url']])
        
def videoDownloads():
    video_url = input("Enter the video's URL: ")
    
    # display tag and video resolution, format, fps
    video = pytube.YouTube(video_url)
    for stream in video.streams:
        if "video" in str(stream) and "mp4" in str(stream) and "True" in str(stream):
            print(stream)

    # tag that correspond to video resolution
    tag = int(input("Enter the tag that coorespond to resolution: "))

    stream = video.streams.get_by_itag(tag)#(22 for res = 720p, fps = 30 and 18 for res = 360p, fps = 30)
    print("Downloading ....")
    stream.download()
    print("Done")
    
def runCode():
    while(True):
        print("""
                1. Download MP3 file.
                2. Download video file
                3. Exit the program
                """)
        key = int(input("Enter your option: "))
        if key == 1: MP3downloads()
        elif key == 2: videoDownloads()
        elif key == 3: exit()
        else: print("Your input is incorrected !!!!")
    
# main function for run all function
if __name__ == "__main__":
    runCode()
